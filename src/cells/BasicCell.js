import Cell from "./Cell"
import FoodCell from "./FoodCell"
import KillerCell from "./KillerCell"

export default class BasicCell extends Cell {
  constructor(scene, x, y, wrapBounds, life) {
    super(scene, x, y, wrapBounds, life)
    this.speed = 2
    this.foodEfficiency = 4
    this.splitTimes = 0
  }

  lifeLoop = () => {
    this.lifeUpdate(-1)

    if (this.splitTimes >= 7) {
      this.sprite.setColors(0xff0000)
      this.speed = 2 * 0.2
    } else if (this.splitTimes >= 5) {
      this.sprite.setColors(0xff4f1f)
      this.speed = 2 * 0.4
    } else if (this.splitTimes >= 3) {
      this.sprite.setColors(0xff954a)
      this.speed = 2 * 0.6
    } else if (this.splitTimes >= 1) {
      this.sprite.setColors(0xffc380)
      this.speed = 2 * 0.8
    }
    
    if (this.life > 60) {
      this.lifeUpdate(-parseInt(this.life / 2))
      const num = Phaser.Math.Between(1, 100)
      if (num > 80) {
        new FoodCell(this.scene, this.x, this.y, this.wrapBounds).setVelocityX(0.5)
      } else if (num < (this.splitTimes - 1) * 3) {
        new KillerCell(this.scene, this.x, this.y, this.wrapBounds, 60).setVelocityX(0.5)
      } else {
        new BasicCell(this.scene, this.x, this.y, this.wrapBounds, this.life).setVelocityX(0.5)
      }
      this.setVelocityX(-0.5)
      this.splitTimes++
    }
  }

  onCollide = (target) => {
    if (target.gameObject instanceof FoodCell) {
      this.lifeUpdate(1)
    }
  }

  onDestroy = () => {
    if (Phaser.Math.Between(3, 10) < this.splitTimes) {
      new KillerCell(this.scene, this.x, this.y, this.wrapBounds, 60)
    }
  }
}