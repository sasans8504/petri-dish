import Cell from "./Cell"
import BasicCell from "./BasicCell"
import Food from "../Food"

export default class FoodCell extends Cell {
  constructor(scene, x, y, wrapBounds, life) {
    super(scene, x, y, wrapBounds, life)
    this.sprite.setColors(0x00a800)
    this.foodEfficiency = 1
  }

  onCollide = (target) => {
    if (this.life > 10) this.fasting = true
    else this.fasting = false
  }

  lifeLoop = () => {
    if (this.life >= 10 && this.life <= 100) {
      this.sprite.setColors(0x00ff00)
      this.lifeUpdate(2)
      new Food(this.scene, this.x, this.y, this.wrapBounds).setVelocity(Phaser.Math.Between(-1, 1), Phaser.Math.Between(-1, 1))
      new Food(this.scene, this.x, this.y, this.wrapBounds).setVelocity(Phaser.Math.Between(-1, 1), Phaser.Math.Between(-1, 1))
      new Food(this.scene, this.x, this.y, this.wrapBounds).setVelocity(Phaser.Math.Between(-1, 1), Phaser.Math.Between(-1, 1))
      new Food(this.scene, this.x, this.y, this.wrapBounds).setVelocity(Phaser.Math.Between(-1, 1), Phaser.Math.Between(-1, 1))
    }

    if (this.life > 100) {
      new BasicCell(this.scene, this.x, this.y, this.wrapBounds).setVelocity(Phaser.Math.Between(-1, 1), Phaser.Math.Between(-1, 1))
      this.selfDestroy()
    }
  }
}