import Cell from "./Cell"

export default class KillerCell extends Cell {
  constructor(scene, x, y, wrapBounds, life) {
    super(scene, x, y, wrapBounds, life)
    this.sprite.setColors(0x03e8fc)
    this.speed = 3
    this.foodEfficiency = 2
  }

  onCollide = (target) => {
    if (target.gameObject instanceof Cell) {
      this.lifeUpdate(target.gameObject.life)
      target.gameObject.lifeSet(-10)
    }
  }

  lifeLoop = () => {
    this.lifeUpdate(-10)

    this.setScale(1 + (this.life / 200), 1 + (this.life / 200))
    
    if (this.life > 500) {
      this.selfDestroy()
    }
  }
}