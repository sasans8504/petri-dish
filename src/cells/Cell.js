import Phaser from "phaser";
import Food from "../Food";
export default class Cell extends Phaser.GameObjects.Container {
  constructor(scene, x, y, wrapBounds, life = 1) {
    super(scene, x, y, [])
    this.cid = scene.randomStr(12)
    this.wrapBounds = wrapBounds
    this.scene = scene
    this.speed = 1
    this.foodEfficiency = 0
    this.fasting = false
    this.life = life
    this.moveable = true
    this.lastUpdateTime = 0
    this.direction = {
      x: 0, y: 0
    }
    this.changeDirection = true
    this.swirlCount = 10
    this.swirlSpeed = 1

    this.sprite = scene.add.rope(0, 0, 'cell')
    this.sprite.anims.play('live')
    this.add(this.sprite)

    this.lifeText = scene.add.text(0, 0, this.life).setOrigin(.5, .5)
    this.add(this.lifeText)
    
    scene.add.existing(this)
    scene.matter.add.gameObject(this, { shape: { type: 'circle', radius: 8}, plugin: wrapBounds})
    this.setIgnoreGravity(true)

    this.setOnCollide((data) => {
      const target = data.bodyB.id === this.body.id ? data.bodyA : data.bodyB
      
      this.onCollide(target)

      if (this.fasting) return
      if (target.gameObject instanceof Food) {
        this.lifeUpdate(this.foodEfficiency)
        scene.registry.get('foods').delete(target.gameObject.fid)
        this.scene.matter.world.remove(target)
        target.gameObject.destroy()
      }
    })

    scene.registry.get('cells').set(this.cid, this)
  }

  lifeUpdate = (point) => {
    if (point > 0) {
      this.changeDirection = false
      this.swirlCount = 10
      this.swirlSpeed = 1
    }
    this.life += point
    this.lifeText.text = this.life
  }

  lifeSet = (point) => {
    this.life = point
    this.lifeText.text = this.life
  }

  selfDestroy() {
    this.onDestroy()
    this.scene.registry.get('cells').delete(this.cid)
    this.moveable = false
    this.destroy()
  }

  update(time) {
    if (time - this.lastUpdateTime > 500) {
      if (this.life - 1 < 0) {
        this.selfDestroy()
        return
      }

      this.lifeLoop()

      if (this.moveable) {
        if (this.changeDirection) {
          this.direction.x = Phaser.Math.FloatBetween(-this.speed, this.speed)
          this.direction.y = Phaser.Math.FloatBetween(-this.speed, this.speed)
        } else if (this.swirlCount > 0) {
          this.direction.x = Math.sin(time) * this.swirlSpeed
          this.direction.y = Math.cos(time) * this.swirlSpeed
          this.swirlSpeed += 0.05
          this.swirlCount--
        }
        this.setVelocity(this.direction.x, this.direction.y)
        // this.applyForce(new Phaser.Math.Vector2(this.direction.x / 1000, this.direction.y / 1000))
      }

      if (this.swirlCount <= 0) {
        this.changeDirection = true
      }

      this.lastUpdateTime = time
    }
  }

  onDestroy() {}
  onCollide(target) {}
  lifeLoop() {}
}