import Cell from "./Cell"

export default class SpiralCell extends Cell {
  constructor(scene, x, y, wrapBounds, life) {
    super(scene, x, y, wrapBounds, life)
    this.sprite.setColors(0xeb28e4)
    this.speed = 1
    this.moveX = 0
    this.moveY = 0
    this.reverseMove = false
  }

  lifeLoop = () => {
    console.log(this.speed)
    this.setVelocity(Math.sin(this.moveX) * this.speed, Math.cos(this.moveY) * this.speed)
    this.moveX++
    this.moveY++
    this.speed += this.reverseMove ? -0.05 : 0.05
    if (this.speed > 2) {
      console.log('reverse')
      this.reverseMove = true
    } else if (this.speed < 0.5) {
      this.reverseMove = false
    }

    this.moveable = false
  }
}