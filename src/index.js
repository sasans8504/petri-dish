import Phaser from "phaser";
import cellSprite from "./assets/cell.png";
import foodSprite from "./assets/food.png";
import FoodCell from "./cells/FoodCell";
import BasicCell from "./cells/BasicCell";
import Food from "./Food";
import SpiralCell from "./cells/SpiralCell";

class GameScene extends Phaser.Scene {
  constructor() {
    super('GameScene')
  }

  preload() {
    this.load.spritesheet('cell', cellSprite, {
      frameWidth: 32, frameHeight: 32
    })
    this.load.image('food', foodSprite)
  }

  create() {
    this.registry.set('cells', new Map())
    this.registry.set('foods', new Map())
    this.anims.create({
      key: 'live',
      frames: this.anims.generateFrameNumbers('cell', { start: 0, end: 7 }),
      frameRate: 6,
      repeat: -1
    })
    this.randomStr = (length) => {
      let result = ''
      const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      const charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
     return result;
    }

    const wrapBounds = {
      wrap: {
        min: {
          x: 0,
          y: 0
        },
        max: {
          x: 800,
          y: 600
        }            
      }
    };

    const foodTimer = this.time.addEvent({
      delay: 10,
      callback: () => {
        new Food(this, Phaser.Math.Between(0, 800), Phaser.Math.Between(0, 600), wrapBounds)
        if (foodTimer.getRepeatCount() === 0) {
            console.log('finish')
          }
        },
      repeat: 99
    })

    const range = 200
    this.time.addEvent({
      delay: 600,
      callback: () => {
        new FoodCell(this, 400 + Phaser.Math.Between(-range, range), 300 + Phaser.Math.Between(-range, range), wrapBounds)
        // new BasicCell(this, 400 + Phaser.Math.Between(-range, range), 300 + Phaser.Math.Between(-range, range), wrapBounds, 10)
      },
      repeat: 9
    })

    // new SpiralCell(this, 400, 300, wrapBounds)

    this.second = 0
    this.minute = 0
    this.hours = 0
    this.restartCount = 11
    this.cellCount = this.add.text(0, 10, `cell:${this.registry.get('cells').size}`).setOrigin(0, .5)
    this.foodCount = this.add.text(0, 30, `food:${this.registry.get('foods').size}`).setOrigin(0, .5)
    this.fpsText = this.add.text(0, 50, 'fps: --').setOrigin(0, .5)
    this.timeText = this.add.text(800, 10, `Time: ${this.hours}:${this.minute}:${this.second}`).setOrigin(1, .5)
    this.restartText = this.add.text(400, 300, `Restart At ${this.restartCount}`, {
      fontSize: 18,
    }).setOrigin(.5, .5).setVisible(false)

    this.time.addEvent({
      delay: 1000,
      callback: () => {
        if (this.restartCount <= 0) {
          this.registry.destroy()
          this.events.off()
          // this.scene.restart()
          location.reload()
          return
        }

        if (this.registry.get('cells').size === 0) {
          this.restartText.setVisible(true)
          this.restartCount--
          this.restartText.text = `Restart At ${this.restartCount}`
          return
        }

        this.cellCount.text = `cell:${this.registry.get('cells').size}`
        this.foodCount.text = `food:${this.registry.get('foods').size}`
        this.second++
        if (this.second >= 60) {
          this.minute++
          this.second = 0
        }
        if (this.minute >= 60) {
          this.hours++
          this.minute = 0
        }
        this.timeText.text = `Time: ${this.hours}:${this.minute}:${this.second}`
      },
      loop: true
    })
  }

  update(time) {
    this.fpsText.text = `fps: ${parseInt(game.loop.actualFps)}`
    if (game.loop.actualFps < 10) {
      location.reload()
    }
    this.registry.get('cells').forEach((cell) => {
      cell.update(time)
    })
  }
}

const config = {
  type: Phaser.AUTO,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  width: 800,
  height: 600,
  backgroundColor: '#000000',
  scene: [
    GameScene,
  ],
  physics: {
    default: 'matter',
    matter: {
      gravity: {
        x: 0.001,
        y: 0
      },
      plugins: {
        wrap: true
      },
      debug: false
    }
  },
};

const game = new Phaser.Game(config);