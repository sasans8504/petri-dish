export default class Food extends Phaser.Physics.Matter.Sprite {
  constructor(scene, x, y, wrapBounds) {
    super(scene.matter.world, x, y, 'food', 0, { plugin: wrapBounds })
    this.fid = scene.randomStr(16)
    this.type = 'food'
    
    scene.add.existing(this)
    scene.registry.get('foods').set(this.fid, this)
  }
}